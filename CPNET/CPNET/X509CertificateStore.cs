﻿namespace CPNET
{
    using System;
    using System.Security.Cryptography.X509Certificates;

    /// <summary>
    /// Provides find certificate methods in X509Store
    /// </summary>
    public static class X509CertificateStore
    {
        /// <summary>
        /// Find certificate in My Store
        /// </summary>
        /// <param name="name">Store name</param>
        /// <param name="thumbprint">Certificate thumbprint</param>
        /// <param name="restrictToMachine">Store location</param>
        /// <returns>Finded certificate or null value</returns>
        /// <exception cref="System.Security.Cryptography.CryptographicException">The store is unreadable</exception>
        /// <exception cref="System.ArgumentNullException">Thumbprint has null value</exception>
        /// <exception cref="System.Security.SecurityException">The caller does not have the required permission</exception>
        public static X509Certificate2 FindFirstCertificate(StoreName name, string thumbprint, bool restrictToMachine = false)
        {
            return FindFirstCertificate(name, thumbprint, restrictToMachine, false);
        }

        /// <summary>
        /// Find certificate using thumbprint
        /// </summary>
        /// <param name="name">Store name</param>
        /// <param name="thumbprint">Ccertificate thumbprint</param>
        /// <param name="restrictToMachine">Store location</param>
        /// <param name="validOnly">Finds only valid certificate</param>
        /// <returns>Finded certificate or null value</returns>
        /// <exception cref="System.Security.Cryptography.CryptographicException">The store is unreadable</exception>
        /// <exception cref="System.ArgumentNullException">Thumbprint has null value</exception>
        /// <exception cref="System.Security.SecurityException">The caller does not have the required permission</exception>
        public static X509Certificate2 FindFirstCertificate(StoreName name, string thumbprint, bool restrictToMachine, bool validOnly)
        {
            if (thumbprint == null)
            {
                throw new ArgumentNullException("thumbprint");
            }
            
            var store = new X509Store(name, restrictToMachine ? StoreLocation.LocalMachine : StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);

            var certificateCollection = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, validOnly);
            var result = certificateCollection.Count == 0 ? null : certificateCollection[0];

            store.Close();
            return result;
        }
    }
}
