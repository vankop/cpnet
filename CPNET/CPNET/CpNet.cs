﻿namespace CPNET
{
    using System;
    using System.Security;
    using System.Security.Cryptography;
    using System.Security.Cryptography.Pkcs;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;

    /// <summary>
    /// Provide verify and sign methods using CMS/PKCS #7 
    /// </summary>
    public static class CpNet
    {
        private const string HashAlgorithm = "GOST3411";

        /// <summary>
        /// Create signature using X509Certificate2
        /// </summary>
        /// <param name="data">Data to signature</param>
        /// <param name="certificate">Signing certificate</param>
        /// <param name="detached">Detached signature if has true value</param>
        /// <returns>Signature message</returns>
        public static byte[] CreateSignature(byte[] data, X509Certificate2 certificate, bool detached = true)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            if (certificate == null)
            {
                throw new ArgumentNullException("certificate");
            }

            var contentInfo = new ContentInfo(data);
            var signedCms = new SignedCms(contentInfo, detached);
            signedCms.ComputeSignature(new CmsSigner(certificate), false);
            return signedCms.Encode();
        }

        /// <summary>
        /// Create signature using input string in Utf8
        /// </summary>
        /// <param name="data">Data to signature</param>
        /// <param name="certificate">Signing certificate</param>
        /// <param name="detached">Detached signature if has true value</param>
        /// <returns>Signature message</returns>
        public static byte[] CreateSignature(string data, X509Certificate2 certificate, bool detached = true)
        {
            return CreateSignature(Encoding.UTF8.GetBytes(data), certificate, detached);
        }

        /// <summary>
        /// Create signature using input string in encoding format
        /// </summary>
        /// <param name="data">Data to signature</param>
        /// <param name="certificate">Signing certificate</param>
        /// <param name="encoding">input string encoding format</param>
        /// <param name="detached">Detached signature if has true value</param>
        /// <returns>Signature message</returns>
        public static byte[] CreateSignature(string data, X509Certificate2 certificate, Encoding encoding, bool detached = true)
        {
            return CreateSignature(encoding.GetBytes(data), certificate, detached);
        }

        /// <summary>
        /// Verify encoded signature
        /// </summary>
        /// <param name="message">Sign message</param>
        /// <param name="signature">Verifing signature</param>
        /// <param name="detached">Detached signature if has true value</param>
        /// <returns>Return true if checked signature succesfully</returns>
        public static bool VerifySignature(byte[] message, byte[] signature, bool detached = true)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            if (signature == null)
            {
                throw new ArgumentNullException("signature");
            }

            var contentInfo = new ContentInfo(message);
            var signedCms = new SignedCms(contentInfo, detached);
            signedCms.Decode(signature);

            try
            {
                signedCms.CheckSignature(true);
            }
            catch (CryptographicException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Verify encoded signature
        /// </summary>
        /// <param name="message">Sign message</param>
        /// <param name="signature">Verifing signature</param>
        /// <param name="messageEncoding">Message encoding</param>
        /// <param name="detached">Detached signature if has true value</param>
        /// <returns>Return true if checked signature succesfully</returns>
        public static bool VerifySignature(string message, byte[] signature, Encoding messageEncoding, bool detached = true)
        {
            return VerifySignature(messageEncoding.GetBytes(message), signature, detached);
        }

        /// <summary>
        /// Verify encoded signature
        /// </summary>
        /// <param name="message">Sign message</param>
        /// <param name="signature">Verifing signature</param>
        /// <param name="messageEncoding">Message encoding</param>
        /// <param name="signatureEncoding">Sgnature encoding</param>
        /// <param name="detached">Detached signature if has true value</param>
        /// <returns></returns>
        public static bool VerifySignature(string message, string signature, Encoding messageEncoding, Encoding signatureEncoding, bool detached = true)
        {
            return VerifySignature(messageEncoding.GetBytes(message), signatureEncoding.GetBytes(signature), detached);
        }

        /// <summary>
        /// Verify encoded signature, return sign certificate
        /// </summary>
        /// <param name="message">Sign message</param>
        /// <param name="signature">Verifing signature</param>
        /// <param name="signingCertificate">Sign certificate</param>
        /// <param name="detached">Detached signature if has true value</param>
        /// <returns>Return true if checked signature succesfully</returns>
        public static bool VerifySignature(byte[] message, byte[] signature, out X509Certificate2 signingCertificate, bool detached = true)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            if (signature == null)
            {
                throw new ArgumentNullException("signature");
            }

            var contentInfo = new ContentInfo(message);
            var signedCms = new SignedCms(contentInfo, detached);
            signedCms.Decode(signature);

            signingCertificate = signedCms.Certificates.Count > 0 ? signedCms.Certificates[0] : null;

            try
            {
                signedCms.CheckSignature(true);
            }
            catch (CryptographicException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get hash from data
        /// </summary>
        /// <param name="data">Input data</param>
        /// <returns>Return created hash</returns>
        /// <exception cref="System.Security.SecurityException">Not installed GOST3411 crypto</exception>
        [STAThread]
        public static byte[] GetHash(byte[] data)
        {
            using (var hash = System.Security.Cryptography.HashAlgorithm.Create(HashAlgorithm))
            {
                if (hash == null)
                    throw new SecurityException("Not installed " + HashAlgorithm + " crypto");

                hash.ComputeHash(data);

                return hash.Hash;
            }
        }

        /// <summary>
        /// Get hash from data
        /// </summary>
        /// <param name="data">Input data</param>
        /// <param name="encoding">Data encoding</param>
        /// <returns>Return created hash</returns>
        /// <exception cref="System.Security.SecurityException">Not installed GOST3411 crypto</exception>
        public static byte[] GetHash(string data, Encoding encoding)
        {
            return GetHash(encoding.GetBytes(data));
        }

        /// <summary>
        /// Get sign information
        /// </summary>
        /// <param name="message">Sign message</param>
        /// <param name="signature">Verifing signature</param>
        /// <param name="detached">Detached signature if has true value</param>
        /// <returns>Return SignInfo</returns>
        public static SignInfo SignInfo(byte[] message, byte[] signature, bool detached = true)
        {
            X509Certificate2 certificate;
            var isCorrect = VerifySignature(message, signature, out certificate, detached);

            return certificate == null ? null : new SignInfo(certificate, isCorrect);
        }
    }
}
