﻿namespace CPNET
{
    using System;
    using System.Security.Cryptography.X509Certificates;

    public class SignInfo
    {
        public bool IsCorrect { get; private set; }

        public string CertHash { get; private set; }

        public string SubName { get; private set; }

        public DateTime NotBefore { get; private set; }

        public DateTime NotAfter { get; private set; }

        public string Issuer { get; private set; }

        public string Thumbprint { get; private set; }

        public SignInfo(X509Certificate2 certificate, bool isCorrect)
            : this(
            isCorrect,
            certificate.GetCertHashString(),
            certificate.Subject,
            certificate.NotBefore,
            certificate.NotAfter,
            certificate.Issuer,
            certificate.Thumbprint)
        {
        }

        public SignInfo(bool isCorrect, string certHash, string subName, DateTime notBefore, DateTime notAfter, string issuer, string thumbprint)
        {
            IsCorrect = isCorrect;
            CertHash = certHash;
            SubName = subName;
            NotBefore = notBefore;
            NotAfter = notAfter;
            Issuer = issuer;
            Thumbprint = thumbprint;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(SignInfo))
                return base.Equals(obj);

            var objt = (SignInfo)obj;

            return
                objt.IsCorrect == this.IsCorrect
                && objt.Issuer == this.Issuer
                && objt.CertHash == this.CertHash
                && objt.SubName == this.SubName
                && objt.NotAfter == this.NotAfter
                && objt.NotBefore == this.NotBefore;
        }
    }
}
