﻿namespace CpNet.Tests
{
    using System;
    using System.Text;
    using System.Security;
    using System.Security.Cryptography.X509Certificates;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using CPNET;

    [TestClass]
    public class CpNetTest
    {
        private const string CertificateThumbPrint = "B35722974E52D15534E292221558AB207B20BAF9";

        [TestMethod]
        public void CreateSignature()
        {
            var message = "create signature";
            var certificate = X509CertificateStore.FindFirstCertificate(StoreName.My, CertificateThumbPrint);
            
            var signatureFromBytes = CpNet.CreateSignature(Encoding.UTF8.GetBytes(message), certificate);

            var signatureFromString = CpNet.CreateSignature(message, certificate);

            var signatureFromEncodingString = CpNet.CreateSignature(message, certificate, Encoding.UTF8);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateSignatureMessageErrorExpected()
        {
            var signatureFromBytes = CpNet.CreateSignature(null, null, Encoding.Unicode);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateSignatureCertificateErrorExpected()
        {
            var signatureFromBytes = CpNet.CreateSignature(string.Empty, null, Encoding.Unicode);
        }

        [TestMethod]
        public void VerifySignature()
        {
            var message = "verify signature";
            var certificate = X509CertificateStore.FindFirstCertificate(StoreName.My, CertificateThumbPrint);

            var signatureFromBytes = CpNet.CreateSignature(Encoding.UTF8.GetBytes(message), certificate);

            var isCorrect = CpNet.VerifySignature(message, signatureFromBytes, Encoding.UTF8);

            Assert.IsTrue(isCorrect, "signature is not correct");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void VerifySignatureSignatureErrorExpected()
        {
            var message = "verify signature";

            CpNet.VerifySignature(message, null, Encoding.UTF8);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void VerifySignatureMessageErrorExpected()
        {
            CpNet.VerifySignature(null, null);
        }

        [TestMethod]
        [ExpectedException(typeof(SecurityException))]
        public void GetHash()
        {
            var message = "get hash";

            var hashFromBytes = CpNet.GetHash(Encoding.UTF8.GetBytes(message));

            var hashFromString = CpNet.GetHash(message, Encoding.UTF8);

            //Assert.AreEqual(hashFromBytes, hashFromString, "hashes are not equal");
        }

        [TestMethod]
        public void SignInfo()
        {
            var message = "sign info";
            var certificate = X509CertificateStore.FindFirstCertificate(StoreName.My, CertificateThumbPrint);

            var signature = CpNet.CreateSignature(message, certificate);

            var signInfoExpected = new SignInfo(true, certificate.GetCertHashString(), certificate.Subject, certificate.NotBefore, certificate.NotAfter, certificate.Issuer, certificate.Thumbprint);

            var singnInfo = CpNet.SignInfo(Encoding.UTF8.GetBytes(message), signature);

            Assert.AreEqual(signInfoExpected, singnInfo, "sign information not eqaul");
        }
    }
}
